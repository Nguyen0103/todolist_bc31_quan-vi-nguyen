const BASE_URL = "https://62b0787ee460b79df0469c94.mockapi.io/todolist";

export let Service = {

    getNote: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    },

    addNote: (note) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: note,
        })
    },

    getCompleteNote: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        })
    },

    completeNote: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: note,
        })
    },

    deleteNote: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        })
    },

}