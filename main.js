import { Controller } from "./Controller/Controller.js";
import { Service } from "./Service/Service.js";

let todoList = [];

let id = null;



let renderTask = (list) => {
    let content = ""

    for (let i = 0; i < list.length; i++) {
        let element = list[i];

        id = element.id

        content +=
            `<li class="task-item">
                    ${element.note} 
                <div class="feature-btn">
                        <i class="fa-solid fa-trash-can" onclick="deleteTask(${element.id})"></i>
                        <i class="fa-solid fa-check" onclick="checkTask(${element.id})"></i>
                </div>
            </li>`
    }
    document.getElementById('todo').innerHTML = content;
};

let renderService = () => {
    Service.getNote()
        .then((res) => {
            todoList = res.data

            renderTask(todoList)
        })
        .catch((err) => {
            console.log('err: ', err);
        });
};
renderService()

//thêm note mới 
document.getElementById('addItem').onclick = () => {

    let newTask = Controller.getNewTask();

    Service.addNote(newTask)
        .then((res) => {

            document.getElementById('newTask').value = ""

            renderService()

        })
        .catch((err) => {
            console.log('err: ', err);
        });
};

//xóa note 
let deleteTask = (id) => {
    Service.deleteNote(id)
        .then((res) => {

            renderService()
        })
        .catch((err) => {
            console.log(err);
        });
}
window.deleteTask = deleteTask

//render lại complete task khi nhấn check
let renderCompleteTask = (list) => {
    let content = ""
    for (let i = 0; i < list.length; i++) {
        let element = id[i];

        content +=
            `<li class="task-item">
                    ${element.note}
            </li>`
    }
    document.getElementById('completed').innerText = content;
}

//check note
let checkTask = (id) => {
    Service.completeNote(id)
        .then((res) => {
            renderService()
            renderCompleteTask(id)
        })
        .catch((err) => {
            console.log(err);
        });
};
window.checkTask = checkTask



